-- Adding new film to film table
INSERT INTO "film" ("title", "description", "release_year",
				  "language_id", "rental_duration", "rental_rate", 
				  "length", "replacement_cost", "rating")
VALUES ('Bridge to terabithia',  2007,
		1, 14, 4.99,
		152, 20.99, 'PG');

-- Adding actors to actor table
INSERT INTO "actor" ("first_name", "last_name") 
VALUES ('Josh', 'Hutcherson'),
       ('Annasophia', 'Roob'),
       ('Bailee', 'Madison')
       
-- Adding film actors to film_actor table
INSERT INTO "film_actor" ("actor_id", "film_id")
VALUES ((SELECT "actor_id" 
		 FROM "actor" 
		 WHERE "first_name" = 'Josh'), 
        (SELECT "film_id" 
		 FROM "film" 
		 WHERE "title" = 'Bridge to terabithia')),
		
       ((SELECT "actor_id" 
		 FROM "actor" 
		 WHERE "first_name" = 'Annasophia'),
        (SELECT "film_id" 
		 FROM "film" 
		 WHERE "title" = 'Bridge to terabithia')),
		
       ((SELECT "actor_id" 
		 FROM "actor" 
		 WHERE "first_name" = 'Bailee'),
        (SELECT "film_id" 
		 FROM "film"
		 WHERE "title" = 'Bridge to terabithia'));
        
-- Adding new film to store by inventory        
INSERT INTO "inventory" ("film_id", "store_id")
VALUES ((
	SELECT "film_id"
	FROM "film"
	WHERE "title" = 'Bridge to terabithia'), 1);
	
	
	
	
	
    SELECT film.title, inventory.store_id, actor.first_name ||' '|| actor.last_name as fullname
	FROM film
	JOIN inventory USING(film_id)
	JOIN film_actor USING(film_id)
	JOIN actor USING(actor_id)
	WHERE film.title LIKE '%terabithia%';